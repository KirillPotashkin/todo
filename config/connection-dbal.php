<?php

use Doctrine\DBAL\DriverManager;

$db_config = require dirname(__DIR__).'/config/database.php';

return DriverManager::getConnection([
    'driver'   => 'pdo_mysql',
    'user'     => $db_config['db_user'],
    'password' => $db_config['db_password'],
    'dbname'   => $db_config['db_name'],
    'host'     => $db_config['db_host'],
]);