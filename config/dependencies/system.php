<?php

use App\Infrastructure\DoctrineStrictObjectManager;
use App\Infrastructure\StrictObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

return [
    StrictObjectManager::class => static function (
        ContainerInterface $container
    ) {
        return new DoctrineStrictObjectManager($container->get('EntityManager'));
    },
    'EntityManager'            => DI\Factory(static function () {
        /* @var EntityManagerInterface $em */
        return require dirname(__DIR__)
                       .'/../config/doctrine-bootstrap.php';
    }),
];