<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

use Doctrine\Migrations\Configuration\Migration\PhpFile;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

/* @var EntityManagerInterface $entityManager */
$entityManager = require dirname(__DIR__)."/config/doctrine-bootstrap.php";


if (isset($argv[0]) && $argv[0] === 'vendor/bin/doctrine') {
    return ConsoleRunner::createHelperSet($entityManager);
}

$config = new PhpFile(dirname(__DIR__).'/migrations/config/migrations.php');

return DependencyFactory::fromEntityManager($config,
    new ExistingEntityManager($entityManager));

