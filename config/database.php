<?php

return [
    'db_name'     => env('DB_NAME'),
    'db_user'     => env('DB_USER'),
    'db_host'     => env('DB_HOST'),
    'db_password' => env('DB_PASSWORD'),
];