<?php

use Doctrine\DBAL\Connection;
use Ramsey\Uuid\Doctrine\UuidType;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDev                     = true;
$proxyDir                  = null;
$cache                     = null;
$useSimpleAnnotationReader = false;

$config = Setup::createAnnotationMetadataConfiguration(
    [
        dirname(__DIR__)."/app/Domain/Entity",
    ],
    $isDev,
    $proxyDir,
    $cache,
    $useSimpleAnnotationReader
);

$config->setProxyDir(dirname(__DIR__).'/storage/proxy');
$config->setAutoGenerateProxyClasses(true);

Type::addType('uuid', UuidType::class);

/* @var Connection $connection */
$connection = require dirname(__DIR__).'/config/connection-dbal.php';

// obtaining the entity manager
return EntityManager::create($connection, $config);