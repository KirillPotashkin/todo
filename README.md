````
composer install
cp .env.example .env
./vendor/bin/doctrine-migrations migration:migrate
php cli/fixture.php
````

User Login data:

````
Login: User
Password: password
````

Admin Login data:

````
Login: Admin
Password: password
````

---

### Pages

- /
- /tasks/{id}
- /admin
- /login