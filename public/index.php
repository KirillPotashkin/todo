<?php

use App\Request\Request;

require_once dirname(__DIR__).'/vendor/autoload.php';

session_start();

/* @var App\App $app */
$app = require dirname(__DIR__).'/di/bootstrap.php';

$route = new \App\Route(
    $app->getContainer(),
    new Request()
);

try {
    $route->dispatchRoute();
} catch (Throwable $ex) {
    $route->sendErrorResponse($ex->getMessage(), 500);
}
