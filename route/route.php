<?php

use App\Controller\AdminController;
use App\Controller\IndexController;
use App\Controller\LoginController;
use App\Controller\TaskController;
use App\Request\Request;
use FastRoute\RouteCollector;

/* @var RouteCollector $route */

$route->addRoute(Request::METHOD_GET, '/',
    IndexController::class.'@index');

$route->addRoute(Request::METHOD_GET,
    '/tasks/{id: [0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}}',
    TaskController::class.'@getById');


$route->addRoute(Request::METHOD_POST,
    '/tasks/{id: [0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}}/done',
    TaskController::class.'@setDone');


$route->addRoute(Request::METHOD_GET, '/admin',
    AdminController::class.'@index');

$route->addRoute(Request::METHOD_POST, '/admin/upload',
    AdminController::class.'@uploadFile');

$route->addRoute(Request::METHOD_GET, '/login',
    LoginController::class.'@index');

$route->addRoute(Request::METHOD_POST, '/login',
    LoginController::class.'@login');