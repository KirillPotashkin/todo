<?php

declare(strict_types=1);

namespace App\Domain\Entity;


use App\Domain\ValueObject\Category\Name;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Category
 *
 * @package App\Domain\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category extends EntityWithId
{

    /**
     * @var \App\Domain\ValueObject\Category\Name
     * @ORM\Embedded(class="\App\Domain\ValueObject\Category\Name",
     *      columnPrefix=false)
     */
    private Name $name;

    /**
     * Category constructor.
     *
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\Category\Name  $name
     */
    protected function __construct(UuidInterface $id, Name $name)
    {
        parent::__construct($id);
        $this->name = $name;
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\Category\Name  $name
     *
     * @return \App\Domain\Entity\Category
     */
    public static function create(UuidInterface $id, Name $name): Category
    {
        return new self($id, $name);
    }

    /**
     * @return \App\Domain\ValueObject\Category\Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

}