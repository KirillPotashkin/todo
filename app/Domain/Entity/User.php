<?php

declare(strict_types=1);

namespace App\Domain\Entity;


use App\Domain\ValueObject\User\Name;
use App\Domain\ValueObject\User\Password;
use App\Domain\ValueObject\User\Role;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="\App\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends EntityWithId
{

    /**
     * @var \App\Domain\ValueObject\User\Name
     * @ORM\Embedded(class="App\Domain\ValueObject\User\Name",
     *      columnPrefix=false)
     */
    private Name $name;

    /**
     * @var \App\Domain\ValueObject\User\Role
     * @ORM\Embedded(class="\App\Domain\ValueObject\User\Role",
     *      columnPrefix=false)
     */
    private Role $role;

    /**
     * @var \App\Domain\ValueObject\User\Password
     * @ORM\Embedded(class="\App\Domain\ValueObject\User\Password",
     *      columnPrefix=false)
     */
    private Password $password;

    /**
     * User constructor.
     *
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\User\Name  $name
     * @param  \App\Domain\ValueObject\User\Role  $role
     * @param  \App\Domain\ValueObject\User\Password  $password
     */
    protected function __construct(
        UuidInterface $id,
        Name $name,
        Role $role,
        Password $password
    ) {
        parent::__construct($id);
        $this->name     = $name;
        $this->role     = $role;
        $this->password = $password;
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\User\Name  $name
     * @param  \App\Domain\ValueObject\User\Role  $role
     * @param  \App\Domain\ValueObject\User\Password  $password
     *
     * @return \App\Domain\Entity\User
     */
    public static function create(
        UuidInterface $id,
        Name $name,
        Role $role,
        Password $password
    ): User {
        return new self($id, $name, $role, $password);
    }

    /**
     * @return \App\Domain\ValueObject\User\Password
     */
    public function getPassword(): Password
    {
        return $this->password;
    }

    /**
     * @return \App\Domain\ValueObject\User\Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return \App\Domain\ValueObject\User\Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

}