<?php


namespace App\Domain\Entity;


use App\Domain\ValueObject\ToDoList\Status;
use App\Domain\ValueObject\ToDoList\ToDoListDescription;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 * @ORM\Table(name="to_do_list")
 */
class ToDoList extends EntityWithId
{

    /**
     * @var \App\Domain\ValueObject\ToDoList\ToDoListDescription
     * @ORM\Embedded(class =
     *     "\App\Domain\ValueObject\ToDoList\ToDoListDescription", columnPrefix
     *     = false)
     */
    private ToDoListDescription $description;

    /**
     * @var \App\Domain\ValueObject\ToDoList\Status
     * @ORM\Embedded(class="\App\Domain\ValueObject\ToDoList\Status", columnPrefix=false)
     */
    private Status $status;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $created_at;


    /**
     * @var \App\Domain\Entity\Category
     * @ORM\ManyToOne(targetEntity="\App\Domain\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private Category $category;

    /**
     * ToDoList constructor.
     *
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\ToDoList\ToDoListDescription  $description
     * @param  \DateTimeImmutable  $created_at
     * @param  \App\Domain\Entity\Category  $category
     */
    protected function __construct(
        UuidInterface $id,
        ToDoListDescription $description,
        DateTimeImmutable $created_at,
        Category $category
    ) {
        parent::__construct($id);

        $this->description = $description;
        $this->status      = Status::create(false);
        $this->created_at  = $created_at;
        $this->category    = $category;
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     * @param  \App\Domain\ValueObject\ToDoList\ToDoListDescription  $description
     * @param  \DateTimeImmutable  $created_at
     * @param  \App\Domain\Entity\Category  $category
     *
     * @return \App\Domain\Entity\ToDoList
     */
    public static function createTask(
        UuidInterface $id,
        ToDoListDescription $description,
        DateTimeImmutable $created_at,
        Category $category
    ): ToDoList {
        return new static($id, $description, $created_at, $category);
    }

    /**
     * @return \App\Domain\ValueObject\ToDoList\ToDoListDescription
     */
    public function getDescription(): ToDoListDescription
    {
        return $this->description;
    }

    /**
     * @return \App\Domain\ValueObject\ToDoList\Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->created_at;
    }

    /**
     * @return \App\Domain\Entity\Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return $this
     */
    public function setDone(): ToDoList
    {
        $this->status = Status::create(true);

        return $this;
    }

}