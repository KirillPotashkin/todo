<?php


namespace App\Domain\Entity;


use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

abstract class EntityWithId
{

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected UuidInterface $id;

    /**
     * EntityWithId constructor.
     *
     * @param  \Ramsey\Uuid\UuidInterface  $id
     */
    protected function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

}