<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Category;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Name
 *
 * @package App\Domain\ValueObject\Category
 * @ORM\Embeddable
 * @psalm-immutable
 */
final class Name
{

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * Name constructor.
     *
     * @param  string  $name
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param  string  $name
     *
     * @return \App\Domain\ValueObject\Category\Name
     */
    public static function create(string $name): Name
    {
        return new self($name);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param  \App\Domain\ValueObject\Category\Name  $other
     *
     * @return bool
     */
    public function equal(self $other): bool
    {
        return $this->name === $other->name;
    }

}