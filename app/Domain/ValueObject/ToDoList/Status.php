<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\ToDoList;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Status
 *
 * @package App\Domain\ValueObject\ToDoList
 * @ORM\Embeddable
 * @psalm-immutable
 */
final class Status
{

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private bool $status;

    /**
     * Status constructor.
     *
     * @param  bool  $done
     */
    private function __construct(bool $done)
    {
        $this->status = $done;
    }

    /**
     * @return bool
     */
    public function status(): bool
    {
        return $this->status;
    }

    /**
     * @param  bool  $done
     *
     * @return \App\Domain\ValueObject\ToDoList\Status
     */
    public static function create(bool $done): Status
    {
        return new Status($done);
    }

    /**
     * @param  \App\Domain\ValueObject\ToDoList\Status  $other
     *
     * @return bool
     */
    public function equal(self $other): bool
    {
        return $this->status === $other->status;
    }

}