<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\ToDoList;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class ToDoListDescription
 *
 * @package App\Domain\ValueObject\ToDoList
 * @ORM\Embeddable
 * @psalm-immutable
 */
final class ToDoListDescription
{

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private string $description;

    /**
     * ToDoListDescription constructor.
     *
     * @param  string  $name
     * @param  string  $description
     */
    private function __construct(string $name, string $description)
    {
        $this->description = trim($description);
        $this->name        = trim($name);
    }

    /**
     * @param  string  $name
     * @param  string  $description
     *
     * @return \App\Domain\ValueObject\ToDoList\ToDoListDescription
     */
    public static function create(
        string $name,
        string $description
    ): ToDoListDescription {
        return new ToDoListDescription($name, $description);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @param  \App\Domain\ValueObject\ToDoList\ToDoListDescription  $other
     *
     * @return bool
     */
    public function equal(self $other): bool
    {
        return $this->name === $other->name
               && $this->description === $other->description;
    }

}