<?php


namespace App\Domain\ValueObject\User;


use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * Class Role
 *
 * @package App\Domain\ValueObject\User
 * @ORM\Embeddable
 */
final class Role
{

    /**
     * @var array<array-key, string>
     */
    private const ROLE
        = [
            'admin' => 'Администратор',
            'user'  => 'Пользователь',
        ];

    /**
     * @var string
     * @ORM\Column(type="string", name="role")
     */
    private string $value;


    /**
     * Role constructor.
     *
     * @param  string  $value
     */
    private function __construct(string $value)
    {
        if ( ! array_key_exists($value, self::ROLE)) {
            throw new InvalidArgumentException('Role '.$value.' is not valid');
        }

        $this->value = $value;
    }

    /**
     * @param  string  $value
     *
     * @return \App\Domain\ValueObject\User\Role
     */
    public static function create(string $value): Role
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function readableValue(): string
    {
        return self::ROLE[$this->value];
    }

    /**
     * @param  \App\Domain\ValueObject\User\Role  $other
     *
     * @return bool
     */
    public function equal(self $other): bool
    {
        return $this->value === $other->value;
    }

}