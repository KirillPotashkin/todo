<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\User;


use App\Exception\HashErrorException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
final class Password
{

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * Password constructor.
     *
     * @param  string  $password
     */
    private function __construct(string $password)
    {
        $hash = password_hash($password, PASSWORD_BCRYPT);

        if ( ! $hash) {
            throw new HashErrorException("Hash error");
        }

        $this->password = $hash;
    }

    /**
     * @param  string  $password
     *
     * @return \App\Domain\ValueObject\User\Password
     */
    public static function create(string $password): Password
    {
        return new self($password);
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->password;
    }


    /**
     * @param  string  $password
     *
     * @return bool
     */
    public function equal(string $password): bool
    {
        return password_verify($password, $this->password);
    }

}