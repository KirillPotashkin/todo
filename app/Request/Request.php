<?php


namespace App\Request;


/**
 * Class Request
 *
 * @package App\Request
 */
class Request extends RequestAbstract
{

    /**
     * @param  string  $type
     * @param  string  $parameter
     *
     * @return string|null
     */
    public function getRequestParam(string $type, string $parameter): ?string
    {
        switch ($type) {
            case static::METHOD_GET:
                $parameterValue = $this->getParamFromGetVar($parameter);
                break;
            case static::METHOD_POST:
                $parameterValue = $this->getParamFromPostVar($parameter);
                break;
            default:
                $parameterValue = null;
        }

        return $parameterValue;
    }

}