<?php

declare(strict_types=1);

namespace App\Request;


/**
 * Class RequestAbstract
 *
 * @package App\Request
 */
abstract class RequestAbstract
{

    /**
     * @var string
     */
    public const METHOD = 'REQUEST_METHOD';

    /**
     * @var string
     */
    public const METHOD_GET = 'GET';

    /**
     * @var string
     */
    public const METHOD_POST = 'POST';

    /**
     * @var string
     */
    public const URI = 'REQUEST_URI';

    /**
     * @var string
     */
    public const SERVER_PROTOCOL = 'SERVER_PROTOCOL';


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $_SERVER[static::METHOD];
    }

    /**
     * @param  string  $type
     * @param  string  $parameter
     *
     * @return string|null
     */
    abstract public function getRequestParam(
        string $type,
        string $parameter
    ): ?string;

    /**
     * @param  string  $parameter
     *
     * @return null|string
     */
    protected function getParamFromGetVar(string $parameter): ?string
    {
        return $this->getParamFromVar($parameter, $_GET);
    }

    /**
     * @param  string  $parameter
     *
     * @return null|string
     */
    protected function getParamFromPostVar(string $parameter): ?string
    {
        return $this->getParamFromVar($parameter, $_POST);
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $_SERVER[static::URI];
    }

    /**
     * @return void
     */
    public function sendMethodNotAllowedHeader(): void
    {
        header($this->getServerProtocol().' 405 Method Not Allowed');
    }

    /**
     * @return void
     */
    public function sendNotFoundHeader(): void
    {
        header($this->getServerProtocol().' 404 Not Found');
    }


    /**
     * @return mixed
     */
    private function getServerProtocol()
    {
        return $_SERVER[static::SERVER_PROTOCOL];
    }

    /**
     * @param  string  $parameter
     * @param  array  $var
     *
     * @return mixed|null
     */
    private function getParamFromVar(string $parameter, array $var)
    {
        return $var[$parameter] ?? null;
    }

}