<?php


namespace App\Exception;


use RuntimeException;

/**
 * Class ServiceException
 *
 * @package App\Exception
 */
class ServiceException extends RuntimeException
{

    /**
     * @var string
     */
    private string $userMessage;

    /**
     * ServiceException constructor.
     *
     * @param  string  $userMessage
     */
    public function __construct(string $userMessage)
    {
        $this->userMessage = $userMessage;
        parent::__construct('Service exception');
    }

    /**
     * @return string
     */
    public function getUserMessage(): string
    {
        return $this->userMessage;
    }

}