<?php

if ( ! function_exists('env')) {
    /**
     * @param  string  $name
     *
     * @return mixed|null
     */
    function env(string $name)
    {
        return $_ENV[$name] ?? null;
    }
}

if ( ! function_exists('app')) {
    /**
     * @return \App\App
     */
    function app(): \App\App
    {
        return \App\App::getInstance();
    }
}

if ( ! function_exists('session')) {
    /**
     * @param  string  $key
     * @param  mixed  $value
     *
     * @return mixed|null
     */
    function session(string $key, $value = null)
    {
        if ( ! $value) {
            return $_SESSION[$key] ?? null;
        }

        $_SESSION[$key] = $value;

        return $_SESSION[$key];
    }
}


if ( ! function_exists('auth')) {
    /**
     * @return \App\Auth
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    function auth(): \App\Auth
    {
        return app()->getContainer()->make(\App\Auth::class);
    }
}