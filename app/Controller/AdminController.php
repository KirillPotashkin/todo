<?php

declare(strict_types=1);

namespace App\Controller;


use App\Services\ToDoListService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class AdminController
 *
 * @package App\Controller
 */
final class AdminController extends AbstractController
{

    /**
     * @var \App\Services\ToDoListService
     */
    private ToDoListService $toDoListService;


    /**
     * AdminController constructor.
     *
     * @param  \App\Services\ToDoListService  $toDoListService
     */
    public function __construct(ToDoListService $toDoListService)
    {
        $this->toDoListService = $toDoListService;
    }


    /**
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(): void
    {
        if ( ! auth()->isAdmin()) {
            $this->redirect('/login');

            return;
        }

        echo $this->render('admin/index');
    }

    /**
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function uploadFile(): void
    {
        if ( ! auth()->isAdmin()) {
            $this->redirect('/login');

            return;
        }


        if ( ! isset($_FILES['data'])) {
            $this->redirect('/admin');

            return;
        }
        $data      = file_get_contents($_FILES['data']['tmp_name']);
        $dataArray = new ArrayCollection(
            array_filter(explode("\r\n", $data))
        );

        $this->toDoListService->saveAdmin($dataArray);

        $this->redirect('/');
    }

}