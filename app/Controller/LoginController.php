<?php

declare(strict_types=1);

namespace App\Controller;


use App\Auth;

/**
 * Class LoginController
 *
 * @package App\Controller
 */
final class LoginController extends AbstractController
{

    /**
     * @var \App\Auth
     */
    private Auth $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(): void
    {
        echo $this->render('login');
    }


    public function login(): void
    {
        if ( ! isset($_POST['login']) && ! isset($_POST['password'])) {
            $this->redirect('/login');
        }

        auth()->login($_POST['login'], $_POST['password']);
        
        $this->redirect('/');
    }

}