<?php


namespace App\Controller;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class AbstractController
 *
 * @package App\Controller
 */
abstract class AbstractController
{

    /**
     * @param  string  $name
     * @param  array  $params
     *
     * @return string
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(string $name, array $params = []): string
    {
        $loader = new FilesystemLoader(
            dirname(__DIR__, 2).'/views'
        );
        $twig   = new Environment($loader);

        $params['user'] = auth()->user();

        return $twig->render($name.'.html.twig', $params);
    }

    /**
     * @param  string  $url
     */
    protected function redirect(string $url): void
    {
        header('Location: '.$url);
    }

}