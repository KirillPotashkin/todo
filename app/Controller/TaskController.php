<?php

declare(strict_types=1);

namespace App\Controller;


use App\Services\ToDoListService;
use Ramsey\Uuid\Uuid;

/**
 * Class TaskController
 *
 * @package App\Controller
 */
final class TaskController extends AbstractController
{

    /**
     * @var \App\Services\ToDoListService
     */
    private ToDoListService $toDoListService;

    /**
     * TaskController constructor.
     *
     * @param  \App\Services\ToDoListService  $toDoListService
     */
    public function __construct(ToDoListService $toDoListService)
    {
        $this->toDoListService = $toDoListService;
    }

    /**
     * @param  string  $id
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getById(string $id): void
    {
        if ( ! auth()->isAuth()) {
            $this->redirect('/');

            return;
        }

        echo $this->render('task/one', [
            'todo' => $this->toDoListService->getById(Uuid::fromString($id)),
        ]);
    }


    /**
     * @param  string  $id
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function setDone(string $id): void
    {
        if ( ! auth()->isAuth()) {
            $this->redirect('/');

            return;
        }

        $todo = $this->toDoListService->setDoneTask(Uuid::fromString($id));

        $this->redirect("/tasks/{$todo->getId()}");
    }

}