<?php

declare(strict_types=1);

namespace App\Controller;


use App\Services\ToDoListService;

/**
 * Class IndexController
 *
 * @package App\Controller
 */
final class IndexController extends AbstractController
{

    /**
     * @var \App\Services\ToDoListService
     */
    private ToDoListService $toDoListService;

    /**
     * IndexController constructor.
     *
     * @param  \App\Services\ToDoListService  $toDoListService
     */
    public function __construct(ToDoListService $toDoListService)
    {
        $this->toDoListService = $toDoListService;
    }


    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(): void
    {
        $todo = $this->toDoListService->getAll();

        echo $this->render('index', [
            'todos' => $todo,
        ]);
    }

}