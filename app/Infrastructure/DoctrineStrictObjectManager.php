<?php

declare(strict_types=1);

namespace App\Infrastructure;


use App\Exception\ServiceException;
use Doctrine\ORM\Decorator\EntityManagerDecorator;

/**
 * Class DoctrineStrictObjectManager
 *
 * @package App\Infrastructure
 */
final class DoctrineStrictObjectManager extends EntityManagerDecorator
    implements StrictObjectManager
{

    /**
     * @inheritDoc
     */
    public function findOrFail(string $entityName, $id): ?object
    {
        $entity = $this->wrapped->find($entityName, $id);

        if ($entity === null) {
            throw new ServiceException(basename(str_replace('\\', '/',
                    $entityName)).' not found');
        }

        return $entity;
    }
}