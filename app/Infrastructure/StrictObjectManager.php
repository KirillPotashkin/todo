<?php


namespace App\Infrastructure;


use App\Exception\ServiceException;
use Doctrine\Persistence\ObjectManager;

/**
 * Interface StrictObjectManager
 *
 * @package App\Infrastructure
 */
interface StrictObjectManager extends ObjectManager
{

    /**
     * @param  string  $entityName
     * @param  mixed  $id
     *
     * @return object|null
     *
     * @template T
     *
     * @psalm-param class-string<T> $entityName
     *
     * @psalm-return T
     *
     * @throws ServiceException
     */
    public function findOrFail(string $entityName, $id): ?object;
}