<?php

declare(strict_types=1);

namespace App\Repository;


use App\Domain\Entity\Category;
use App\Domain\ValueObject\Category\Name;
use Doctrine\ORM\EntityRepository;

final class CategoryRepository extends EntityRepository
{

    /**
     * @param  \App\Domain\ValueObject\Category\Name  $name
     *
     * @return Category|null
     */
    public function getByName(Name $name): ?Category
    {
        /* @var Category|null $cat */
        $cat = $this->findOneBy([
            'name.name' => $name->name(),
        ]);

        return $cat;
    }

}