<?php


namespace App\Repository;


use App\Domain\Entity\User;
use App\Domain\ValueObject\User\Name;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 *
 * @package App\Repository
 */
final class UserRepository extends EntityRepository
{


    /**
     * @param  \App\Domain\ValueObject\User\Name  $name
     *
     * @return \App\Domain\Entity\User|null
     */
    public function getByName(Name $name): ?User
    {
        /* @var \App\Domain\Entity\User|null $user */
        $user = $this->findOneBy([
            'name.name' => $name->name(),
        ]);

        return $user;
    }

}