<?php

declare(strict_types=1);

namespace App\Repository;


use App\Domain\Entity\ToDoList;
use App\Services\DTO\ToDoListDTO;
use Doctrine\ORM\EntityRepository;

final class ToDoListRepository extends EntityRepository
{

    /**
     * @return array<ToDoList>
     */
    public function getAll(): array
    {
        return $this->getEntityManager()
                    ->createQueryBuilder()
                    ->select('t')
                    ->from(ToDoList::class, 't')
                    ->where("t.status.status=:status")
                    ->setParameter('status', false)
                    ->orderBy('t.created_at')
                    ->getQuery()
                    ->getResult();
    }


    /**
     * @param  \App\Services\DTO\ToDoListDTO  $dto
     *
     * @return bool
     */
    public function exist(ToDoListDTO $dto): bool
    {
        return (bool) $this->findOneBy([
            'description.name'        => $dto->getDescription()->name(),
            'description.description' => $dto->getDescription()->description(),
            'created_at'              => $dto->getDate(),
        ]);
    }

}