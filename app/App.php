<?php

declare(strict_types=1);

namespace App;

use DI\Container;
use DI\ContainerBuilder;

/**
 * Class App
 */
final class App
{

    /**
     * @var \App\App|null $this
     */
    private static ?self $instance = null;

    /**
     * @var \DI\Container
     */
    private Container $container;

    /**
     * App constructor.
     *
     * @throws \Exception
     */
    private function __construct()
    {
        $builder = new ContainerBuilder();

        $builder->useAutowiring(true);
        $builder->useAnnotations(false);

        $builder->addDefinitions(require dirname(__DIR__)
                                         .'/di/dependencies.php');

        $this->container = $builder->build();
    }

    /**
     * @return \App\App
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return \DI\Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }


    /**
     * @param  string  $class
     *
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function make(string $class)
    {
        return $this->container->get($class);
    }

}