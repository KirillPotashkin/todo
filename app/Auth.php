<?php

declare(strict_types=1);

namespace App;


use App\Domain\Entity\User;
use App\Exception\LoginFailException;
use App\Services\LoginService;
use Ramsey\Uuid\Nonstandard\Uuid;

/**
 * Class Auth
 *
 * @package App
 */
final class Auth
{

    /**
     * @var \App\Services\LoginService
     */
    private LoginService $loginService;

    /**
     * Auth constructor.
     *
     * @param  \App\Services\LoginService  $loginService
     */
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    /**
     * @param  string  $login
     * @param  string  $password
     *
     * @return \App\Domain\Entity\User
     * @throws LoginFailException
     */
    public function login(string $login, string $password): User
    {
        $user = $this->loginService->findByLogin($login);

        if ( ! $user) {
            throw new LoginFailException("Login invalid");
        }

        if ( ! $user->getPassword()->equal($password)) {
            throw new LoginFailException("Password invalid");
        }

        session('id', $user->getId()->toString());

        return $user;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->loginService->isAdmin(Uuid::fromString(session('id')));
    }

    /**
     * @return bool
     */
    public function isAuth(): bool
    {
        return (bool) session('id');
    }

    /**
     * @return \App\Domain\Entity\User|null
     */
    public function user(): ?User
    {
        if (session('id')) {
            return $this->loginService->getUser(Uuid::fromString(session('id')));
        }

        return null;
    }

}