<?php

declare(strict_types=1);

namespace App\Services;


use App\Domain\Entity\Category;
use App\Domain\ValueObject\Category\Name;
use App\Infrastructure\StrictObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class CategoryService
 *
 * @package App\Services
 */
final class CategoryService
{

    /**
     * @var \App\Infrastructure\StrictObjectManager
     */
    protected StrictObjectManager $entityManager;

    /**
     * ToDoListService constructor.
     *
     * @param  \App\Infrastructure\StrictObjectManager  $entityManager
     */
    public function __construct(StrictObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param  \App\Domain\ValueObject\Category\Name  $name
     *
     * @return \App\Domain\Entity\Category
     */
    public function create(Name $name): Category
    {
        /* @var $rep \App\Repository\CategoryRepository */
        $rep = $this->entityManager->getRepository(Category::class);

        if ( ! $category = $rep->getByName($name)) {
            $category = Category::create(Uuid::uuid4(), $name);
            $this->entityManager->persist($category);
            $this->entityManager->flush();
        }

        return $category;
    }

}