<?php

declare(strict_types=1);

namespace App\Services;


use App\Domain\Entity\ToDoList;
use App\Domain\ValueObject\Category\Name;
use App\Domain\ValueObject\ToDoList\ToDoListDescription;
use App\Infrastructure\StrictObjectManager;
use App\Services\DTO\ToDoListDTO;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ToDoListService
 *
 * @package App\Services
 */
final class ToDoListService
{

    /**
     * @var \App\Infrastructure\StrictObjectManager
     */
    protected StrictObjectManager $entityManager;


    protected CategoryService $categoryService;

    /**
     * ToDoListService constructor.
     *
     * @param  \App\Infrastructure\StrictObjectManager  $entityManager
     * @param  \App\Services\CategoryService  $categoryService
     */
    public function __construct(
        StrictObjectManager $entityManager,
        CategoryService $categoryService
    ) {
        $this->entityManager   = $entityManager;
        $this->categoryService = $categoryService;
    }


    /**
     * @return array<ToDoList>
     */
    public function getAll(): array
    {
        /* @var $rep \App\Repository\ToDoListRepository */
        $rep = $this->entityManager->getRepository(ToDoList::class);

        return $rep->getAll();
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     *
     * @return ToDoList|null
     */
    public function getById(UuidInterface $id): ?ToDoList
    {
        /* @var $todo ToDoList */
        $todo = $this->entityManager
            ->findOrFail(ToDoList::class, $id);

        return $todo;
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     *
     * @return \App\Domain\Entity\ToDoList
     */
    public function setDoneTask(UuidInterface $id): ToDoList
    {
        /* @var $todo ToDoList */
        $todo = $this->entityManager
            ->findOrFail(ToDoList::class, $id);

        $todo->setDone();

        $this->entityManager->persist($todo);
        $this->entityManager->flush();

        return $todo;
    }


    /**
     * @param  \Doctrine\Common\Collections\ArrayCollection  $data
     *
     * @return array<ToDoList>
     */
    public function saveAdmin(ArrayCollection $data): array
    {
        /* @var $repToDo \App\Repository\ToDoListRepository */
        $repToDo = $this->entityManager->getRepository(ToDoList::class);

        /* @var ArrayCollection|ToDoListDTO[] $dtos */
        $dtos = $data->map(function (string $value) {
            return $this->getDtosWithString($value);
        });

        $batchSize = 0;
        $todos     = [];

        foreach ($dtos as $dto) {
            $category = $this->categoryService
                ->create($dto->getNameCategory());

            if (!$repToDo->exist($dto)) {
                $todo = TodoList::createTask(
                    Uuid::uuid4(),
                    $dto->getDescription(),
                    $dto->getDate(),
                    $category
                );

                $this->entityManager->persist($todo);
                $todos[] = $todo;
            }

            if ($batchSize > 10) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }

            $batchSize++;
        }

        $this->entityManager->flush();
        $this->entityManager->clear();

        return $todos;
    }


    /**
     * @param  string  $value
     *
     * @return \App\Services\DTO\ToDoListDTO
     * @throws \Exception
     */
    private function getDtosWithString(string $value): ToDoListDTO
    {
        $data = explode(';', rtrim($value, ';'));
        preg_match('/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})[+-](\d{2}):(\d{2})/',
            $data[0], $dateFind);

        $dataCatName
            = explode(';',
            preg_replace('/ (\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})[+-](\d{2}):(\d{2}) /',
                ';', $data[0]));

        return new ToDoListDTO(
            ToDoListDescription::create(
                $dataCatName[1],
                $data[1]
            ),
            new DateTimeImmutable($dateFind[0]),
            Name::create($dataCatName[0])
        );
    }

}