<?php

declare(strict_types=1);

namespace App\Services;


use App\Domain\Entity\User;
use App\Domain\ValueObject\User\Name;
use App\Domain\ValueObject\User\Role;
use App\Exception\ServiceException;
use App\Infrastructure\StrictObjectManager;
use Ramsey\Uuid\UuidInterface;

/**
 * Class LoginService
 *
 * @package App\Services
 */
final class LoginService
{

    /**
     * @var \App\Infrastructure\StrictObjectManager
     */
    private StrictObjectManager $objectManager;

    /**
     * LoginService constructor.
     *
     * @param  \App\Infrastructure\StrictObjectManager  $objectManager
     */
    public function __construct(StrictObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param  string  $login
     *
     * @return \App\Domain\Entity\User|null
     */
    public function findByLogin(string $login): ?User
    {
        /* @var \App\Repository\UserRepository $rep */
        $rep = $this->objectManager->getRepository(User::class);

        return $rep->getByName(Name::create($login));
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     *
     * @return bool
     */
    public function isAdmin(UuidInterface $id): bool
    {
        /** @var User $user */
        $user = $this->objectManager->findOrFail(User::class, $id);

        return $user->getRole()->equal(Role::create('admin'));
    }

    /**
     * @param  \Ramsey\Uuid\UuidInterface  $id
     *
     * @return \App\Domain\Entity\User|null
     */
    public function getUser(UuidInterface $id): ?User
    {
        /* @var $user User */
        try {
            $user = $this->objectManager->findOrFail(User::class, $id);
        } catch (ServiceException $exception) {
            $user = null;
        }

        return $user;
    }

}