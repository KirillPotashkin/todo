<?php


namespace App\Services\DTO;


use App\Domain\ValueObject\Category\Name;
use App\Domain\ValueObject\ToDoList\ToDoListDescription;
use DateTimeImmutable;

final class ToDoListDTO
{

    /**
     * @var \App\Domain\ValueObject\ToDoList\ToDoListDescription
     */
    private ToDoListDescription $description;
    private DateTimeImmutable $date;
    /**
     * @var \App\Domain\ValueObject\Category\Name
     */
    private Name $nameCategory;

    /**
     * ToDoListDTO constructor.
     *
     * @param  \App\Domain\ValueObject\ToDoList\ToDoListDescription  $description
     * @param  \DateTimeImmutable  $date
     * @param  \App\Domain\ValueObject\Category\Name  $nameCategory
     */
    public function __construct(
        ToDoListDescription $description,
        DateTimeImmutable $date,
        Name $nameCategory
    ) {
        $this->description  = $description;
        $this->date         = $date;
        $this->nameCategory = $nameCategory;
    }

    /**
     * @return \App\Domain\ValueObject\ToDoList\ToDoListDescription
     */
    public function getDescription(): ToDoListDescription
    {
        return $this->description;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return \App\Domain\ValueObject\Category\Name
     */
    public function getNameCategory(): Name
    {
        return $this->nameCategory;
    }

}