<?php

declare(strict_types=1);


namespace App;


use App\Request\RequestAbstract;
use DI\Container;
use FastRoute\Dispatcher;

use FastRoute\RouteCollector;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

use function FastRoute\simpleDispatcher;

/**
 * Class Route
 *
 * @package App
 */
final class Route
{

    /**
     * @var string
     */
    private const HANDLER_DELIMITER = '@';

    /**
     * @var \DI\Container
     */
    private Container $container;

    /**
     * @var \App\Request\RequestAbstract
     */
    private RequestAbstract $request;

    /**
     * Request constructor.
     *
     * @param  \DI\Container  $container
     * @param  \App\Request\RequestAbstract  $request
     */
    public function __construct(Container $container, RequestAbstract $request)
    {
        $this->container = $container;
        $this->request   = $request;
    }


    /**
     * @param  string  $requestMethod
     * @param  string  $requestUri
     * @param  Dispatcher  $dispatcher
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    private function dispatch(
        string $requestMethod,
        string $requestUri,
        Dispatcher $dispatcher
    ): void {
        $routeInfo = $dispatcher->dispatch($requestMethod, $requestUri);

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $this->request->sendNotFoundHeader();
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $this->request->sendMethodNotAllowedHeader();
                break;
            case Dispatcher::FOUND:
                [$state, $handler, $vars] = $routeInfo;

                if (is_callable($handler)) {
                    call_user_func_array($handler, $vars);
                    unset($state);
                    break;
                }

                [$class, $method] = explode(self::HANDLER_DELIMITER, $handler,
                    2);

                $controller = $this->container->get($class);
                $controller->{$method}(...array_values($vars));

                unset($state);
                break;
        }
    }

    /**
     * @return void
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function dispatchRoute(): void
    {
        $requestMethod = $this->request->getMethod();
        $requestUri    = $this->request->getUri();

        if ($pos = (strpos($requestUri, '?') !== false)) {
            $requestUri = substr($requestUri, 0, (int) $pos);
        }

        $requestUri = rawurldecode($requestUri);

        $this->dispatch($requestMethod, $requestUri, $this->getDispatch());
    }


    /**
     * @return \FastRoute\Dispatcher
     */
    private function getDispatch(): Dispatcher
    {
        return simpleDispatcher(function (RouteCollector $route) {
            require_once dirname(__DIR__).'/route/route.php';
        });
    }

    /**
     * @param  string  $messages
     * @param  int  $httpCode
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendErrorResponse(string $messages, int $httpCode): void
    {
        http_response_code($httpCode);
        echo $messages;
    }


    /**
     * @param  string  $messages
     * @param  int  $httpCode
     *
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function renderError(string $messages, int $httpCode): string
    {
        $loader = new FilesystemLoader(
            dirname(__DIR__).'/views/errors'
        );
        $twig   = new Environment($loader);


        return $twig->render($httpCode.'.html.twig', [
            'errors' => $messages,
        ]);
    }

}