<?php

$file = array_merge(
    glob(dirname(__DIR__).'/config/dependencies/*.php') ?: [],
);

$config = array_map(
    static function ($file) {
        /* @psalm-suppress  UnresolvableInclude */
        return require $file;
    }, $file
);


return array_merge_recursive(...$config);