<?php

use Dotenv\Dotenv;

$app = app();

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();


return $app;