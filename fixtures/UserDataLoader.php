<?php

declare(strict_types=1);

namespace Fixtures;


use App\Domain\Entity\User;
use App\Domain\ValueObject\User\Name;
use App\Domain\ValueObject\User\Password;
use App\Domain\ValueObject\User\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class UserDataLoader
 *
 * @package Fixtures
 */
final class UserDataLoader implements FixtureInterface
{

    /**
     * @param  \Doctrine\Persistence\ObjectManager  $manager
     */
    public function load(ObjectManager $manager): void
    {
        $user = User::create(
            Uuid::uuid4(),
            Name::create('User'),
            Role::create('user'),
            Password::create('password')
        );

        $manager->persist($user);
        $manager->flush();
    }

}