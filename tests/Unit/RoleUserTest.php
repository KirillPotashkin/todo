<?php

declare(strict_types=1);

namespace Test\Unit;


use App\Domain\ValueObject\User\Role;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class RoleUserTest extends TestCase
{

    public function testCreateAdminRole(): void
    {
        $role = Role::create('admin');

        self::assertEquals('admin', $role->value());
    }

    public function testCreateUserRole(): void
    {
        $role = Role::create('user');

        self::assertEquals('user', $role->value());
    }

    public function testErrorRole(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Role::create('tester');
    }

}