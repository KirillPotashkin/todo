<?php

use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

require dirname(__DIR__).'/vendor/autoload.php';

/* @var \App\App $app */
$app = require dirname(__DIR__).'/di/bootstrap.php';

$loader = new Loader();

$loader->loadFromDirectory(dirname(__DIR__).'/fixtures');


$purger = new ORMPurger();

$executor = new ORMExecutor($app->getContainer()->make('EntityManager'),
    $purger);

$executor->execute($loader->getFixtures());
